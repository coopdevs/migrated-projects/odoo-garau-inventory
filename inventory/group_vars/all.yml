---
# Use python3 interpreter
ansible_python_interpreter: "/usr/bin/python3"

# Odoo vars
odoo_role_odoo_user: odoo

# Odoo role
odoo_role_download_strategy: "tar"
odoo_role_odoo_version: "14.0"
odoo_role_odoo_release: "14.0_2021-09-01"
odoo_role_odoo_url: "https://gitlab.com/coopdevs/OCB/-/archive/{{ odoo_role_odoo_release }}/OCB-{{ odoo_role_odoo_release }}.tar.gz"
odoo_role_python_version: "3.8.12"
odoo_role_limit_time_cpu: 6000
odoo_role_limit_time_real: 12000

# Odoo provisioning
odoo_provisioning_version: "v0.7.0"

# Enables the web db manager, needed by backups
odoo_role_list_db: true

odoo_role_workers: 4

# Nginx configuration
nginx_configs:
  upstream:
    - upstream odoo { server 127.0.0.1:8069; }

nginx_sites:
  odoo:
    - |
      listen 80;
      server_name {{ domains | default([inventory_hostname]) | join(' ') }};
      rewrite ^(.*)$ https://$host$1 permanent;
  odoo.ssl:
    - |
      listen 443 ssl;
      ssl_certificate /etc/letsencrypt/live/{{ inventory_hostname }}/fullchain.pem;
      ssl_certificate_key /etc/letsencrypt/live/{{ inventory_hostname }}/privkey.pem;
      include /etc/letsencrypt/options-ssl-nginx.conf;
      ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
      server_name {{ domains | default([inventory_hostname]) | join(' ') }};
      proxy_read_timeout 720s;
      proxy_connect_timeout 720s;
      proxy_send_timeout 720s;
      proxy_set_header X-Forwarded-Host $host;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Real-IP $remote_addr;
      ssl on;
      access_log /var/log/nginx/odoo.access.log;
      error_log /var/log/nginx/odoo.error.log;
      location / {
        proxy_redirect off;
        proxy_pass http://odoo;
      }
      location /longpolling/ {
        proxy_pass http://127.0.0.1:8072;

        proxy_next_upstream error timeout invalid_header http_500 http_502 http_503 http_504;
        proxy_redirect off;

        proxy_set_header    Host            $host;
        proxy_set_header    X-Real-IP       $remote_addr;
        proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Proto https;
      }
      
      gzip_types text/css text/less text/plain text/xml application/xml application/json application/javascript;
      gzip on;


odoo_role_odoo_core_modules_dict:
  shared:
    - account
    - analytic
    - auth_signup
    - base
    - base_automation
    - base_iban
    - base_import
    - base_setup
    - base_vat
    - contacts
    - crm
    - l10n_es
    - maintenance
    - mrp
    - point_of_sale
    - product
    - project
    - purchase
    - repair
    - sale_management
    - stock
    - web
    - web_kanban_gauge
    - website_sale

    # - hr
    # - hr_attendance
    # - hr_expense
    # - hr_recruitment
    # - hr_timesheet
    # - survey
odoo_role_odoo_community_modules_dict:
  shared:
    - account_banking_mandate
    - account_banking_pain_base
    - account_banking_sepa_credit_transfer
    - account_banking_sepa_direct_debit
    - account_due_list
    - account_financial_report
    - account_fiscal_year
    - account_payment_mode
    - account_payment_order
    - account_payment_partner
    - account_payment_return
    - account_reconcile_reconciliation_date
    - account_reconciliation_widget
    - base_bank_from_iban
    - base_iso3166
    - base_technical_features
    - contract
    - contract_sale
    - l10n_es_account_asset
    - l10n_es_account_statement_import_n43
    - l10n_es_aeat
    - l10n_es_aeat_mod111
    - l10n_es_aeat_mod115
    - l10n_es_aeat_mod303
    - l10n_es_aeat_mod349
    - l10n_es_mis_report
    - l10n_es_partner
    - l10n_es_toponyms
    - mis_builder
    - mis_builder_budget
    - project_category
    - project_list
    - project_parent_task_filter
    - project_status
    - project_task_add_very_high
    - project_task_dependency
    - project_task_material
    - project_template
    - project_timeline
    - project_timeline_task_dependency
    - project_timesheet_time_control
    - web_company_color
    - web_responsive
    - web_no_bubble

    # - queue_job
    # - helpdesk_mgmt
    # - helpdesk_mgmt_project
    # - helpdesk_motive
    # - helpdesk_type
    # - hr_attendance_report_theoretical_time
    # - hr_course
    # - hr_employee_medical_examination
    # - hr_holidays_public
    # - mass_mailing_partner
    # - project_task_default_stage
    # - project_timesheet_time_control